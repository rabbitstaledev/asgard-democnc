define({ "api": [
  {
    "type": "get",
    "url": "content/:posttype/categories",
    "title": "Get Categories list",
    "description": "<p>Get list of Categories by Posttype (e.g. blog, news, etc.)</p>",
    "version": "0.1.0",
    "name": "GetCategories",
    "group": "Content",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Content Translation Locale</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "posttype",
            "description": "<p>Content Posttype</p>"
          }
        ]
      }
    },
    "filename": "Modules/Content/Http/Controllers/Api/CategoryController.php",
    "groupTitle": "Content"
  },
  {
    "type": "get",
    "url": "content/:posttype/categories/:id",
    "title": "Get Category detail",
    "description": "<p>Get Category Detail by unique ID</p>",
    "version": "0.1.0",
    "name": "GetCategory",
    "group": "Content",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Content Post Translation Locale</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "posttype",
            "description": "<p>Content Posttype</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Category unique ID</p>"
          }
        ]
      }
    },
    "filename": "Modules/Content/Http/Controllers/Api/CategoryController.php",
    "groupTitle": "Content"
  },
  {
    "type": "get",
    "url": "content/:posttype/posts/:id",
    "title": "Get Post detail",
    "description": "<p>Get Post Detail by unique ID</p>",
    "version": "0.1.0",
    "name": "GetPost",
    "group": "Content",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Content Post Translation Locale</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "posttype",
            "description": "<p>Content Posttype</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Post unique ID</p>"
          }
        ]
      }
    },
    "filename": "Modules/Content/Http/Controllers/Api/PostController.php",
    "groupTitle": "Content"
  },
  {
    "type": "get",
    "url": "content/:posttype/posts",
    "title": "Get Posts list",
    "description": "<p>Get list of Posts by Posttype (e.g. blog, news, etc.)</p>",
    "version": "0.1.0",
    "name": "GetPosts",
    "group": "Content",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Content Translation Locale</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "posttype",
            "description": "<p>Content Posttype</p>"
          }
        ],
        "Query string": [
          {
            "group": "Query string",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>Page Number</p>"
          },
          {
            "group": "Query string",
            "type": "Number",
            "optional": true,
            "field": "perPage",
            "defaultValue": "10",
            "description": "<p>Items Per Page</p>"
          },
          {
            "group": "Query string",
            "type": "String",
            "optional": true,
            "field": "orderBy",
            "defaultValue": "id",
            "description": "<p>OrderBy Field</p>"
          },
          {
            "group": "Query string",
            "type": "String",
            "optional": true,
            "field": "sortOrder",
            "defaultValue": "desc",
            "description": "<p>Sort Ordering</p>"
          },
          {
            "group": "Query string",
            "type": "String",
            "optional": true,
            "field": "categoryId",
            "defaultValue": "",
            "description": "<p>Filter by CategoryId</p>"
          }
        ]
      }
    },
    "filename": "Modules/Content/Http/Controllers/Api/PostController.php",
    "groupTitle": "Content"
  }
] });
